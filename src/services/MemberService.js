import axios from 'axios';
import {API_BASE_URL} from "../Constans";

class MemberService {
    getMembers() {
        return axios.get(API_BASE_URL + "/members");
    }
}

export default new MemberService()