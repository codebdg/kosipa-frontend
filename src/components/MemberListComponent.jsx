import React, {Component} from "react";
import MemberService from "../services/MemberService";

class MemberListComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            members: []
        }
        this.addMember = this.addMember.bind(this);
        this.editMember = this.editMember.bind(this);
        this.deleteMember = this.deleteMember.bind(this);
    }

    componentDidMount() {
        MemberService.getMembers().then((res) => {
            this.setState({members: res.data});
        });
    }

    addMember() {

    }

    editMember(id) {

    }

    deleteMember(id) {

    }

    viewMember(id) {

    }

    render() {
        return (
            <div>
                <h2 className="text-center">Member List</h2>
                <div className="row">
                    <button className="btn btn-primary" onClick={this.addMember}>Add Member</button>
                </div>
                <br></br>
                <div className="row">
                    <table className="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Member Name</th>
                            <th>Member DOB</th>
                            <th>Member Address</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.members.map(member =>
                                <tr key={member.id}>
                                    <td>{member.name}</td>
                                    <td>{member.dob}</td>
                                    <td>{member.address}</td>
                                    <td>
                                        <button onClick={() => this.editMember(member.id)}
                                                className="btn btn-info">Update
                                        </button>
                                        <button style={{marginLeft: "10px"}}
                                                onClick={() => this.deleteMember(member.id)}
                                                className="btn btn-danger">Delete
                                        </button>
                                        <button style={{marginLeft: "10px"}} onClick={() => this.viewMember(member.id)}
                                                className="btn btn-info">View
                                        </button>
                                    </td>
                                </tr>
                            )
                        }
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default MemberListComponent