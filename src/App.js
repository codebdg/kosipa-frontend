import React from "react";
import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import HeaderComponent from "./components/HeaderComponent";
import FooterComponent from "./components/FooterComponent";
import MemberListComponent from "./components/MemberListComponent";
import MemberCreateUpdateComponent from "./components/MemberCreateUpdateComponent";

function App() {
    return (
        <div className="App">
            <Router>
                <HeaderComponent/>
                <div className="container">
                    <Switch>
                        <Route path="/" exact component={MemberListComponent}></Route>
                        <Route path="/members" component={MemberListComponent}></Route>
                        <Route path="/member-create-update/:id" component={MemberCreateUpdateComponent}></Route>
                    </Switch>
                </div>
                <FooterComponent/>
            </Router>
        </div>
    );
}

export default App;
